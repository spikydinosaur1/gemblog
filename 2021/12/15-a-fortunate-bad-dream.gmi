# A fortunate bad dream

I woke up in the middle of last night after a bad dream, and as strange as this might sound, I was fortunate to do so.

You see, I had to host and present a webinar over Zoom this evening. The content has been done for a while and tried to be clever by recording it as a video, so the plan was to start the webinar, play the video and then answer any questions at the end. Simple!

But I woke up at half past one in the morning with a dread feeling that it would go wrong, and then thought: will the audio be transmitted over Zoom?

By default, no. Uh-oh. Asking around, there is an option to enable it so I did a practice run ... and the option didn't work.

If I hadn't had that dream, I wouldn't have discovered any of this until the webinar started and then I would have had to deal with it there and then, which isn't a pleasant thought. My backup plan was to use the script that I'd previously prepared and do the whole thing "live", which happily worked out OK.

Lucky escape!
