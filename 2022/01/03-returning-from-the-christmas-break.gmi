# Returning from the Christmas break

As is often the case, breaking up from work for the holidays didn't mean an abundance of free time. There are always things to do to fill the day.

Family was a big part of it, and being away from home meant no writing on my Gemini capsule. My setup is somewhat primitive and I can't connect to my host to upload new content. I have tentative plans to sort this out by emailing in new posts, having some backend system put them in place and then updating my atom feed. I also read that this is exactly the kind of thing that the Titan protocol is for, so it's worth reading up on that.

I did manage to get a little bit of work done on my Gemini server. Mime types are now detected in a simplistic way. For half an hour I looked at using the Clojure Pantomime library for this, which wraps Apache Tika. While this would do everything I could want, the size of the project ballooned in sized by a factor of 5 so I fell back to my original approach which I think will do fine for now. Maybe as the whole thing grows I'll reconsider.

Taking a break is supposed to let you come back recharged but I don't feel that at the moment. I rather enjoyed not having to think about work life. It seems that I need to make some changes, but with a long tenure with the same company and a responsibility to my family there's a lot of impedance to overcome.
