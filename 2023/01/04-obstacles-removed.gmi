# Obstacles removed

In my last post, a ridiculous 5 months ago, I was lamenting on not keeping up with my blogging because I didn't have sufficient tooling to make it easy. What a terrible excuse.

Over the holidays I made time to finish up some code to create an atom feed from my blog entries. Alas, my coding skills are a bit rusty these days and it took longer than expected but it's there, and if you can read this, it works!
